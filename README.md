# README #
### IMPORTANT!!!! ###
The Master branch has broken code. The last commit was supposed to go to the Work branch but instead went to the Master Branch. If you want to pull the master branch, pull the Work branch.

### Understanding the code ###

There are two distinct modes of play in the program that have many similarities. 

# STANDARD #
In this mode, keys are interchangable (the model only checks the number of keys a player has picked up). The doors are numbered 
and in order to get through one door the player must have picked up keys equal to the number of the door.

# ANCESTOR #
In this mode, keys (Ancestor Names) correspond one to one with doors. In order to move through any given door, the player must
enter in the name of the person whose face is displayed on the door. The door can be opened even if its corresponding key has
not been picked up by the player.

## ARCHITECTURE ##

# Game Modes Separation with the Abstract Factory Pattern #
In the case of the architecture of the program, the standard and ancestor game modes require different object types. Because
of this, the model utilizes the abstract factory pattern. The only component a user creating the model needs to have selected
in this case is the concrete factory that specifies the model objects.

# Model Creation #
The factory cannot generate a model straight out as the data necessary for creating the model is complicated and would otherwise
require a lot of repeated code. To rectify this, the factory creates a model builder that separates the model building, such as
the map and the player, from the procedural generation of the maze.

The map is procedurally generated inside of the StandardMazeCreator class. This class requires access to the factory and it
returns the completed model once the model is created.