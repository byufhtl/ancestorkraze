///<reference path="viewJavascriptDeclarations.d.ts"/>
///<reference path="htmlView.ts"/>
///<reference path="../Main/KrazeClient.ts"/>
/**
 * Created by phobos2390 on 8/10/15.
 */

var enterName = function()
{
  var answer = document.getElementById("doorAnswer");
  var name = answer.value;
  client.enterName(name);
};

var leavePopup = function()
{
  client.leavePopup();
};

var addKey = function()
{

};

var startGame = function()
{
  if(!started)
  {
    started = true;
    var logElement = document.getElementById("log");
    logElement.value = "Done!";
    client = Main.KrazeClient.create("ancestor",getHeight(),getWidth());
    client.setGender(gender);
    setElementDisplay("differentVersions","none");
    setElementDisplay("viewScreen","inline");
    setElementVisibility("canvas-container","visible");
    setElementVisibility("messages","visible");
    if(numberOfPeopleWithPictures == 0)
    {
      logElement.value = "Did not load any ancestors. This can be due to there not being enough " +
      "information or it could be due to other problems. Try restarting by pressing ESC. In the " +
      "meantime, check out the version with Mormon Historical Figures.";
    }
    else
    {
      logElement.value = "";
    }
  }
};
